<?php
include 'includes/session.php';
include 'includes/header.php';


//https://www.w3schools.com/php/func_date_date_format.asp
// echo date("l"); //fulltext sunday- monday
// echo date("d"); //26
// echo date("t"); //31
// echo date("j"); //31
// echo date("F"); //maret
// echo date("m"); //01-13
// echo date("Y"); //2022

$getMonth = date("F");
$months = array("January", "February", "March", "April", "May", "Juni", "July", "August", "September", "October", "November", "December");

$month = "";
$month_query = "";
$month_number = 1;
if (isset($_GET['month'])) {
    $month = $_GET['month'];
    $month_query = "'$_GET[month]'";
    $month_number += array_search($_GET['month'], $months);
} else {
    $month = date("F");
    $month_query = "DATE_FORMAT(CURRENT_DATE(),'%M')";
    $month_number += array_search(date("F"), $months);
}

$days = cal_days_in_month(CAL_GREGORIAN, $month_number, date("Y")); // days of month

?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'includes/navbar.php'; ?>
        <?php include 'includes/menubar.php'; ?>

        <div class="content-wrapper">
            <section class="content-header">
                <h1>Attendance <b><?php echo date("Y") ?></b></h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Attendance</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <?php
                if (isset($_SESSION['error'])) {
                    echo "
                        <div class='alert alert-danger alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-warning'></i> Error!</h4>
                        " . $_SESSION['error'] . "
                        </div>
                    ";
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo "
                        <div class='alert alert-success alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-check'></i> Success!</h4>
                        " . $_SESSION['success'] . "
                        </div>
                    ";
                    unset($_SESSION['success']);
                }
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <div class="pull-left">
                                    <?php
                                    for ($i = 0; $i < count($months); $i++) {
                                        if ($months[$i] == $month) {
                                            echo "<a href='attendance.php?month=" . $months[$i] . "' class='btn btn-warning btn-sm btn-flat'>" . $months[$i] . "</a>";
                                        } else {
                                            echo "<a href='attendance.php?month=" . $months[$i] . "' class='btn btn-primary btn-sm btn-flat'>" . $months[$i] . "</a>";
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="pull-right">
                                    <a href="#addnew" data-toggle="modal" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New Absent</a>
                                </div>
                            </div>
                            <div class="box-body">
                                <table id="attendance1" class="table table-bordered">
                                    <thead>
                                        <th style='width:200px;'>Employee Name</th>
                                        <?php
                                        for ($i = 0; $i < $days; $i++) {
                                            echo "<th style='width:25px;'>" . (1 + $i) . "</th>";
                                        }
                                        ?>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT * FROM employees";
                                        $query = $conn->query($sql);
                                        while ($row = $query->fetch_assoc()) {
                                            echo "<tr>";
                                            echo "<td style='width:200px;'><a href='attendance_detail.php?employee_id=$row[id]&month=$month'><span class='fa fa-external-link'></span> $row[firstname] $row[lastname]</a></td>";
                                            for ($i = 0; $i < $days; $i++) {
                                                $sql2 = "SELECT count(*) AS jml FROM attendance WHERE employee_id=$row[id] AND DATE_FORMAT(date,'%e')=($i + 1) AND DATE_FORMAT(date,'%M')=$month_query";
                                                $query2 = $conn->query($sql2);
                                                $row2 = $query2->fetch_assoc();

                                                if ($row2['jml'] > 0) {
                                                    echo "<td style='width:25px;'><i class='fa fa-circle fa-lg text-success'></i></td>";
                                                } else {
                                                    // echo "<td style='width:25px;'><i class='fa fa-bed text-yellow'></i></td>";
                                                    // echo "<td style='width:25px;'><i class='fa fa-ban text-danger'></i></td>";
                                                    echo "<td style='width:25px;'></td>";
                                                }
                                            }
                                            echo "</tr>";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer">
                                <ul class="list-unstyled">
                                    <li>Keterangan :</li>
                                    <li><i class='fa fa-circle text-success'></i> Hadir</li>
                                    <li><i class='fa fa-warning text-yellow'></i> Sakit/Izin</li>
                                    <li><i class='fa fa-ban text-red'></i> Cuti</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/attendance_modal.php'; ?>
    </div>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>