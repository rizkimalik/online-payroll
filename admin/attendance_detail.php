<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'includes/navbar.php'; ?>
        <?php include 'includes/menubar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Attendance
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Attendance</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- <div class="box-header with-border">
                                <a href="#addnew" data-toggle="modal" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
                            </div> -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered">
                                    <thead>
                                        <th class="hidden"></th>
                                        <th>Date</th>
                                        <th>Employee ID</th>
                                        <th>Name</th>
                                        <th>Start Working</th>
                                        <th>Finish Working</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT attendance.*, 
                                            employees.employee_id AS empid,
                                            employees.firstname,
                                            employees.lastname
                                        FROM attendance 
                                        LEFT JOIN employees ON employees.id=attendance.employee_id 
                                        WHERE attendance.employee_id='$_GET[employee_id]' AND DATE_FORMAT(attendance.date,'%M')='$_GET[month]'
                                        ORDER BY attendance.date ASC";

                                        $query = $conn->query($sql);
                                        while ($row = $query->fetch_assoc()) {
                                            $status = ($row['status']) ? '<span class="label label-warning pull-right">ontime</span>' : '<span class="label label-danger pull-right">late</span>';
                                            echo "
                                                <tr>
                                                <td class='hidden'></td>
                                                <td>" . date('M d, Y', strtotime($row['date'])) . "</td>
                                                <td>" . $row['empid'] . "</td>
                                                <td>" . $row['firstname'] . ' ' . $row['lastname'] . "</td>
                                                <td>" . date('h:i A', strtotime($row['time_in'])) . $status . "</td>
                                                <td>" . date('h:i A', strtotime($row['time_out'])) . "</td>
                                                </tr>
                                            ";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php include 'includes/footer.php'; ?>
    </div>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>