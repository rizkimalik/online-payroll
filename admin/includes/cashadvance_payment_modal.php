<div class="modal fade" id="cash_payment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><b>Cash Advance Payment</b></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="POST" action="cashadvance_payment_actions.php">
                    <input type="hidden" class="form-control" name="employee_id">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">No Document</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="code_ca" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Employee</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="employee_name" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Remaining Pay</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="remaining_pay" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Amount Payment</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="amount_payment" required>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                <button type="submit" class="btn btn-primary btn-flat" name="add_payment"><i class="fa fa-save"></i> Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>