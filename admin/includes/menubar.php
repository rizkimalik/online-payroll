<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo (!empty($user['photo'])) ? '../images/' . $user['photo'] : '../images/profile.jpg'; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $user['firstname'] . ' ' . $user['lastname']; ?></p>
                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN</li>
            <li><a href="home.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li><a href="attendance.php"><i class="fa fa-calendar"></i> <span>Attendance</span></a></li>
            
            <li class="header">EMPLOYEE</li>
            <li><a href="employee.php"><i class="fa fa-group"></i> Employee List</a></li>
            <li><a href="position.php"><i class="fa fa-suitcase"></i> Positions</a></li>
            <li><a href="schedule.php"><i class="fa fa-calendar"></i> Schedules</a></li>
            <li><a href="schedule_employee.php"><i class="fa fa-circle-o"></i> <span>Employee Schedule</span></a></li>

            <li class="header">PAYROLL</li>
            <!-- <li><a href="salary.php"><i class="fa fa-credit-card"></i> Salary Management</a></li> -->
            <li><a href="overtime.php"><i class="fa fa-clock-o"></i> Overtime</a></li>
            <li><a href="cashadvance.php"><i class="fa fa-files-o"></i> Cash Advance</a></li>
            <li><a href="deduction.php"><i class="fa fa-file-text"></i> Deductions</a></li>
            <li><a href="payroll.php"><i class="fa fa-money"></i> <span>Payroll</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>