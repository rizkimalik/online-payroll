<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'includes/navbar.php'; ?>
        <?php include 'includes/menubar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Cash Advance 
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li>Employees</li>
                    <li class="active">Cash Advance</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <?php
                if (isset($_SESSION['error'])) {
                    echo "
                        <div class='alert alert-danger alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-warning'></i> Error!</h4>
                        " . $_SESSION['error'] . "
                        </div>
                    ";
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo "
                        <div class='alert alert-success alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-check'></i> Success!</h4>
                        " . $_SESSION['success'] . "
                        </div>
                    ";
                    unset($_SESSION['success']);
                }
                ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <a href="#addnew" data-toggle="modal" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
                            </div>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered">
                                    <thead>
                                        <th class="hidden"></th>
                                        <th>Code Cash Advance</th>
                                        <th>Date</th>
                                        <th>Name</th>
                                        <th>Amount</th>
                                        <th>Remaining Pay</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT cashadvance.id, 
                                            cashadvance.code_ca, 
                                            cashadvance.amount, 
                                            cashadvance.date_advance,
                                            cashadvance.remaining_pay,
                                            cashadvance.status, 
                                            employees.employee_id, 
                                            employees.firstname, 
                                            employees.lastname 
                                        FROM cashadvance 
                                        LEFT JOIN employees ON employees.id=cashadvance.employee_id 
                                        -- WHERE DATE_FORMAT(date_advance,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')  
                                        ORDER BY date_advance DESC";

                                        $query = $conn->query($sql);
                                        while ($row = $query->fetch_assoc()) {
                                            $status = $row['status'] == 'paidoff' ? '<span class="label label-success">Paid Off</span>' : '<span class="label label-warning">Pending</span>';
                                            $disabled = $row['status'] == 'paidoff' ? 'disabled' : '';
                                            $disabled2 = $row['amount'] == $row['remaining_pay'] ? '' : 'disabled';

                                            echo "
                                                <tr>
                                                <td class='hidden'></td>
                                                <td><a href='cashadvance_payment.php?code_ca=$row[code_ca]' target='_blank'><span class='fa fa-external-link'></span> " . $row['code_ca'] . "</a></td>
                                                <td>" . date('M d, Y', strtotime($row['date_advance'])) . "</td>
                                                <td>" . $row['firstname'] . ' ' . $row['lastname'] . "</td>
                                                <td>Rp. " . number_format($row['amount'], 2) . "</td>
                                                <td>Rp. " . number_format($row['remaining_pay'], 2) . "</td>
                                                <td>$status</td>
                                                <td>
                                                    <button class='btn btn-info btn-sm payment btn-flat' data-id='$row[id]' $disabled><i class='fa fa-money'></i> Pay Debt</button>
                                                    <button class='btn btn-success btn-sm edit btn-flat' data-id='$row[id]' $disabled $disabled2><i class='fa fa-edit'></i> Edit</button>
                                                    <button class='btn btn-danger btn-sm delete btn-flat' data-id='$row[id]'><i class='fa fa-trash'></i> Delete</button>
                                                </td>
                                                </tr>
                                            ";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/cashadvance_modal.php'; ?>
        <?php include 'includes/cashadvance_payment_modal.php'; ?>
    </div>
    <?php include 'includes/scripts.php'; ?>
    <script>
        $(function() {
            $('.payment').click(function(e) {
                e.preventDefault();
                $('#cash_payment').modal('show');
                var id = $(this).data('id');
                console.log(id)
                getRow(id);
            });
            
            $('.edit').click(function(e) {
                e.preventDefault();
                $('#edit').modal('show');
                var id = $(this).data('id');
                getRow(id);
            });

            $('.delete').click(function(e) {
                e.preventDefault();
                $('#delete').modal('show');
                var id = $(this).data('id');
                getRow(id);
            });
        });

        function getRow(id) {
            $.ajax({
                type: 'POST',
                url: 'cashadvance_actions.php',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(response) {
                    $('.date').html(response.date_advance);
                    $('.employee_name').val(response.firstname + ' ' + response.lastname);
                    $('.caid').val(response.caid);
                    $('#edit_amount').val(response.amount);

                    $('[name=employee_id]').val(response.employee_id);
                    $('[name=code_ca]').val(response.code_ca);
                    $('[name=employee_name]').val(response.firstname + ' ' + response.lastname);
                    $('[name=remaining_pay]').val(response.remaining_pay);
                }
            });
        }
    </script>
</body>

</html>