<?php
include 'includes/session.php';

if (isset($_POST['add'])) {
    $title = $_POST['title'];
    $salary = $_POST['salary'];
    $rate = $_POST['rate'];
    $bonus = $_POST['bonus'];

    $sql = "INSERT INTO position (description, rate,salary,bonus) VALUES ('$title', '$rate', '$salary', '$bonus')";
    if ($conn->query($sql)) {
        $_SESSION['success'] = 'Position added successfully';
    } else {
        $_SESSION['error'] = $conn->error;
    }

    header('location: position.php');
} else if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $title = $_POST['title'];
    $salary = $_POST['salary'];
    $rate = $_POST['rate'];
    $bonus = $_POST['bonus'];

    $sql = "UPDATE position SET description = '$title', salary = '$salary', rate = '$rate', bonus = '$bonus' WHERE id = '$id'";
    if ($conn->query($sql)) {
        $_SESSION['success'] = 'Position updated successfully';
    } else {
        $_SESSION['error'] = $conn->error;
    }

    header('location: position.php');
} else if (isset($_POST['delete'])) {
    $id = $_POST['id'];
    $sql = "DELETE FROM position WHERE id = '$id'";
    if ($conn->query($sql)) {
        $_SESSION['success'] = 'Position deleted successfully';
    } else {
        $_SESSION['error'] = $conn->error;
    }
    
    header('location: position.php');
} else if (isset($_POST['id'])) { //?show detail
    $id = $_POST['id'];
    $sql = "SELECT * FROM position WHERE id = '$id'";
    $query = $conn->query($sql);
    $row = $query->fetch_assoc();

    echo json_encode($row);
} else {
    $_SESSION['error'] = 'action error.';
    header('location: position.php');
}
