<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'includes/navbar.php'; ?>
        <?php include 'includes/menubar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Payslip
                </h1>
            </section>
            <div class="row">
                <div class="col-xs-12">
                    <button type="button" class="btn btn-primary pull-right" onclick="printJS({ 
                                            printable: 'cetak', 
                                            type: 'html', 
                                            scanStyles: true,
                                            css: ['../bower_components/bootstrap/dist/css/bootstrap.min.css','../dist/css/AdminLTE.min.css'],
                                            documentTitle: ''
                                        })">
                        <i class="fa fa-download"></i> Generate PDF
                    </button>
                </div>
            </div>

            <!-- Main content -->
            <section class="invoice" id="cetak">
                <?php
                $sql = "SELECT SUM(amount) as total_amount FROM deductions";
                $query = $conn->query($sql);
                $drow = $query->fetch_assoc();
                $deduction = $drow['total_amount'];

                $sql = "SELECT  SUM(attendance.num_hr) AS total_hours, 
                    (
                        SELECT COUNT(*) as jml FROM attendance as a
                        WHERE a.employee_id=attendance.employee_id AND DATE_FORMAT(a.date,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')
                    ) AS total_attends,
                    attendance.employee_id AS empid, employees.employee_id, employees.firstname, employees.lastname,  position.description AS position,position.rate, position.salary, position.bonus
                    FROM attendance 
                    LEFT JOIN employees ON employees.id=attendance.employee_id 
                    LEFT JOIN position ON position.id=employees.position_id 
                    WHERE DATE_FORMAT(attendance.date,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')
                    GROUP BY attendance.employee_id 
                    ORDER BY employees.lastname ASC, employees.firstname ASC";

                $query = $conn->query($sql);
                $total = 0;
                while ($row = $query->fetch_assoc()) {
                    $employee_id = $row['empid'];

                    $casql = "SELECT SUM(amount_payment) AS cashamount FROM cashadvance_payment WHERE employee_id='$employee_id' AND DATE_FORMAT(date_payment,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
                    $caquery = $conn->query($casql);
                    $carow = $caquery->fetch_assoc();
                    $cashadvance = $carow['cashamount'];

                    $otsql = "SELECT SUM(rate) AS cashovertime FROM overtime WHERE employee_id='$employee_id' AND DATE_FORMAT(date_overtime,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
                    $otquery = $conn->query($otsql);
                    $otrow = $otquery->fetch_assoc();
                    $overtime = $otrow['cashovertime'];

                    $otsql2 = "SELECT SUM(hours) AS hours_overtime FROM overtime WHERE employee_id='$employee_id' AND DATE_FORMAT(date_overtime,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
                    $otquery2 = $conn->query($otsql2);
                    $otrow2 = $otquery2->fetch_assoc();
                    $total_overtime = $otrow2['hours_overtime'] == '' ? 0 : $otrow2['hours_overtime'];

                    $salary = $row['salary'];
                    $gross = $row['rate'] * $row['total_attends'];
                    $total_deduction = $deduction + $cashadvance;
                    $bonus = ($salary + $gross) * $row['bonus'] / 100;
                    $net = ($salary + $gross + $overtime + $bonus) - $total_deduction;
                ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header">
                                Payslip <?php echo date('F') ?>
                                <small class="pull-right"><?php echo date('d F Y'); ?></small>
                            </h2>
                        </div>
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            <!-- <b>Payslip #20220400</b><br> -->
                            <br> 
                            <b>Name :</b> <?php echo $row['firstname'] . " " . $row['lastname']; ?><br>
                            <b>Position :</b> <?php echo $row['position']; ?><br>
                            <b>Payment :</b> <?php echo date('F') . " " . date('Y') ?><br>
                            <b>Attendance :</b> <?php echo $row['total_attends'] . " days"; ?><br>
                            <br>
                            <br>
                        </div>

                    </div>


                    <!-- <div class="row"> -->
                    <!-- <div class="col-xs-6">
                        <p class="lead">Payment Methods:</p>
                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                        </p>
                    </div> -->

                    <h2 class="lead">Detail Payslip</h2>
                    <table class="table">
                        <tbody>
                            <tr>
                                <th style="width:50%">Salary Amount :</th>
                                <td>Rp. <?php echo number_format($salary, 2); ?></td>
                            </tr>
                            <tr>
                                <th>Gross Amount :</th>
                                <td>Rp. <?php echo number_format($gross, 2); ?></td>
                            </tr>
                            <tr>
                                <th>Percentage Bonus (<?php echo $row['bonus'] ?> %) :</th>
                                <td>Rp. <?php echo number_format($bonus, 2); ?></td>
                            </tr>
                            <tr>
                                <th>Deductions :</th>
                                <td>Rp. <?php echo number_format($deduction, 2); ?></td>
                            </tr>
                            <tr>
                                <th>Cash Advance :</th>
                                <td>Rp. <?php echo number_format($cashadvance, 2); ?></td>
                            </tr>
                            <tr>
                                <th>Overtime (<?php echo $total_overtime; ?> hours) :</th>
                                <td>Rp. <?php echo number_format($overtime, 2); ?></td>
                            </tr>
                            <tr>
                                <th>Net Pay :</th>
                                <td>Rp. <?php echo number_format($net, 2); ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                    <!-- <div class="row"> -->
                    <!-- <div class="col-lg-8"></div> -->
                    <!-- <div class="col-lg-4"> -->

                    <span class="pull-right">Hormat Kami,</span>

                    <!-- </div> -->
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <!-- </div> -->
                    <!-- </div> -->
                <?php } ?>
            </section>
        </div>

        <?php include 'includes/footer.php'; ?>
    </div>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>