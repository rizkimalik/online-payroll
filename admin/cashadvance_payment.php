<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'includes/navbar.php'; ?>
        <?php include 'includes/menubar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Cash Advance Payment
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Cash Advance Payment</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <!-- <div class="box-header with-border">
                                <a href="#addnew" data-toggle="modal" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
                            </div> -->
                            <div class="box-body">
                                <table id="example1" class="table table-bordered">
                                    <thead>
                                        <th class="hidden"></th>
                                        <th>Code Payment</th>
                                        <th>Code Cash Advance</th>
                                        <th>Datetime</th>
                                        <th>Amount</th>
                                        <th>Amount Payment</th>
                                        <th>Remaining Pay</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT * FROM cashadvance_payment WHERE code_ca='$_GET[code_ca]' ORDER BY date_payment ASC";

                                        $query = $conn->query($sql);
                                        while ($row = $query->fetch_assoc()) {
                                            $status = $row['status'] == 'paidoff' ? '<span class="label label-success">Paid Off</span>' : '<span class="label label-warning">Pending</span>';

                                            echo "
                                                <tr>
                                                <td class='hidden'></td>
                                                <td>" . $row['code_pm'] . "</td>
                                                <td>" . $row['code_ca'] . "</td>
                                                <td>" . $row['date_payment'] . "</td>
                                                <td>Rp. " . number_format($row['amount'], 2) . "</td>
                                                <td>Rp. " . number_format($row['amount_payment'], 2) . "</td>
                                                <td>Rp. " . number_format($row['amount'] - $row['amount_payment'], 2) . "</td>
                                                <td>" . $status . "</td>
                                                </tr>
                                            ";
                                            // <td>Rp. " . number_format($row['remaining_pay'], 2) . "</td>

                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php include 'includes/footer.php'; ?>
    </div>
    <?php include 'includes/scripts.php'; ?>
   
</body>

</html>