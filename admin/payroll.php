<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'includes/navbar.php'; ?>
        <?php include 'includes/menubar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Payroll
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Payroll</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <?php
                if (isset($_SESSION['error'])) {
                    echo "
                        <div class='alert alert-danger alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-warning'></i> Error!</h4>
                        " . $_SESSION['error'] . "
                        </div>
                    ";
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['success'])) {
                    echo "
                        <div class='alert alert-success alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-check'></i> Success!</h4>
                        " . $_SESSION['success'] . "
                        </div>
                    ";
                    unset($_SESSION['success']);
                }
                ?>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <div class="pull-left">
                                    <h3 class="box-title">Monthly Payroll - <b><?php echo date("F") . " " . date("Y") ?></b></h3>
                                </div>
                                <div class="pull-right">
                                    <form method="POST" class="form-inline" id="payForm">
                                        <button type="button" class="btn btn-success btn-sm btn-flat" id="payroll"><span class="glyphicon glyphicon-print"></span> Payroll</button>
                                        <!-- <button type="button" class="btn btn-primary btn-sm btn-flat" onclick="printJS({ 
                                            printable: 'cetak', 
                                            type: 'html', 
                                            scanStyles: true,
                                            // css: ['assets/css/bootstrap.min.css'],
                                            documentTitle: ''
                                        })">
                                            <span class="glyphicon glyphicon-print"></span> Payslip
                                        </button> -->
                                        <button type="button" class="btn btn-primary btn-sm btn-flat" id="payslip"><span class="glyphicon glyphicon-print"></span> Payslip</button>
                                    </form>
                                </div>
                            </div>
                            <div class="box-body" id="cetak">
                                <table id="example1" class="table table-bordered">
                                    <thead>
                                        <th>Employee Name</th>
                                        <th>Position</th>
                                        <th>Total Attendance</th>
                                        <th>Total Overtime</th>
                                        <th>Percentage Bonus</th>
                                        <th>Salary Amount</th>
                                        <th>Gross Amount</th>
                                        <th>Overtime</th>
                                        <th>Bonus</th>
                                        <th>Deductions</th>
                                        <th>Cash Advance</th>
                                        <th>Net Pay</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT SUM(amount) as total_amount FROM deductions";
                                        $query = $conn->query($sql);
                                        $drow = $query->fetch_assoc();
                                        $deduction = $drow['total_amount'];

                                        $sql = "SELECT  SUM(attendance.num_hr) AS total_hours, 
                                        (
                                            SELECT COUNT(*) as jml FROM attendance as a
                                            WHERE a.employee_id=attendance.employee_id AND DATE_FORMAT(a.date,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')
                                        ) AS total_attends,
                                        attendance.employee_id AS empid, employees.employee_id, employees.firstname, employees.lastname,  position.description AS position,position.rate, position.salary, position.bonus
                                        FROM attendance 
                                        LEFT JOIN employees ON employees.id=attendance.employee_id 
                                        LEFT JOIN position ON position.id=employees.position_id 
                                        WHERE DATE_FORMAT(attendance.date,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')
                                        GROUP BY attendance.employee_id 
                                        ORDER BY employees.lastname ASC, employees.firstname ASC";

                                        $query = $conn->query($sql);
                                        $total = 0;
                                        while ($row = $query->fetch_assoc()) {
                                            $employee_id = $row['empid'];

                                            $casql = "SELECT SUM(amount_payment) AS cashamount FROM cashadvance_payment WHERE employee_id='$employee_id' AND DATE_FORMAT(date_payment,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
                                            $caquery = $conn->query($casql);
                                            $carow = $caquery->fetch_assoc();
                                            $cashadvance = $carow['cashamount'];

                                            $otsql = "SELECT SUM(rate) AS cashovertime FROM overtime WHERE employee_id='$employee_id' AND DATE_FORMAT(date_overtime,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
                                            $otquery = $conn->query($otsql);
                                            $otrow = $otquery->fetch_assoc();
                                            $overtime = $otrow['cashovertime'];

                                            $otsql2 = "SELECT SUM(hours) AS hours_overtime FROM overtime WHERE employee_id='$employee_id' AND DATE_FORMAT(date_overtime,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
                                            $otquery2 = $conn->query($otsql2);
                                            $otrow2 = $otquery2->fetch_assoc();
                                            $total_overtime = $otrow2['hours_overtime'] == '' ? 0 : $otrow2['hours_overtime'];

                                            $salary = $row['salary'];
                                            $gross = $row['rate'] * $row['total_attends'];
                                            $total_deduction = $deduction + $cashadvance;
                                            $bonus = ($salary + $gross) * $row['bonus'] / 100;
                                            $net = ($salary + $gross + $overtime + $bonus) - $total_deduction;

                                            echo "
                                                <tr>
                                                <td>" . $row['firstname'] . " " . $row['lastname'] . "</td>
                                                <td>" . $row['position'] . "</td>
                                                <td>" . $row['total_attends'] . " days</td>
                                                <td>" . $total_overtime . " hours</td>
                                                <td>" . $row['bonus'] . " %</td>
                                                <td>Rp. " . number_format($salary, 2) . "</td>
                                                <td>Rp. " . number_format($gross, 2) . "</td>
                                                <td>Rp. " . number_format($overtime, 2) . "</td>
                                                <td>Rp. " . number_format($bonus, 2) . "</td>
                                                <td>Rp. " . number_format($deduction, 2) . "</td>
                                                <td>Rp. " . number_format($cashadvance, 2) . "</td>
                                                <td>Rp. " . number_format($net, 2) . "</td>
                                                </tr>
                                            ";
                                        }

                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <?php include 'includes/footer.php'; ?>
    </div>
    <?php include 'includes/scripts.php'; ?>
    <script>
        $(function() {
            $('.edit').click(function(e) {
                e.preventDefault();
                $('#edit').modal('show');
                var id = $(this).data('id');
                getRow(id);
            });

            $('.delete').click(function(e) {
                e.preventDefault();
                $('#delete').modal('show');
                var id = $(this).data('id');
                getRow(id);
            });

            $("#reservation").on('change', function() {
                var range = encodeURI($(this).val());
                window.location = 'payroll.php?range=' + range;
            });

            $('#payroll').click(function(e) {
                e.preventDefault();
                $('#payForm').attr('action', 'payroll_generate.php');
                $('#payForm').submit();
            });

            $('#payslip').click(function(e) {
                e.preventDefault();
                $('#payForm').attr('action', 'payslip_generate.php');
                $('#payForm').submit();
            });

        });

        function getRow(id) {
            $.ajax({
                type: 'POST',
                url: 'position_row.php',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(response) {
                    $('#posid').val(response.id);
                    $('#edit_title').val(response.description);
                    $('#edit_rate').val(response.rate);
                    $('#del_posid').val(response.id);
                    $('#del_position').html(response.description);
                }
            });
        }
    </script>
</body>

</html>