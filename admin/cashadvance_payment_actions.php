<?php
include 'includes/session.php';

if (isset($_POST['add_payment'])) {
    $code_ca = $_POST['code_ca'];
    $employee_id = $_POST['employee_id'];
    $remaining_pay = $_POST['remaining_pay'];
    $amount_payment = $_POST['amount_payment'];
    $amount_remaining_pay = $remaining_pay - $amount_payment;

    if ($amount_remaining_pay >= 0) {
        if ($amount_remaining_pay == 0) {
            $status = 'paidoff';
        } else {
            $status = 'pending';
        }

        $sqlcode = "SELECT RIGHT(code_pm,5) as code FROM cashadvance_payment ORDER BY id DESC LIMIT 1";
        $querycode = $conn->query($sqlcode);
        if ($querycode->num_rows <> 0) {
            $rowcode = $querycode->fetch_assoc();
            $code = $rowcode['code'] + 1;
        } else {
            $code = 1;
        }

        $tahun = date("y");
        $bulan = date("m");
        $get_id = str_pad($code, 5, "0", STR_PAD_LEFT);
        $code_pm = "PM$tahun$bulan$get_id";

        $sql = "INSERT INTO cashadvance_payment (code_pm, code_ca,employee_id, date_payment, amount, amount_payment, remaining_pay, status) 
        VALUES ('$code_pm','$code_ca','$employee_id', NOW(), '$remaining_pay', '$amount_payment', '$amount_remaining_pay', '$status')";
        
        if ($conn->query($sql)) {
            $sqlupdate = "UPDATE cashadvance SET remaining_pay = '$amount_remaining_pay', status = '$status' WHERE code_ca = '$code_ca'";
            if ($conn->query($sqlupdate)) {
                $_SESSION['success'] = 'Payment added successfully';
            }
        } else {
            $_SESSION['error'] = $conn->error;
        }
    } else {
        $_SESSION['error'] = 'Amount payment bigger than expected.';
    }
} else {
    $_SESSION['error'] = 'Fill up add form first';
}

header('location: cashadvance.php');
