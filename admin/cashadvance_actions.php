<?php
include 'includes/session.php';

if (isset($_POST['add'])) {
    $employee = $_POST['employee'];
    $amount = $_POST['amount'];

    $sql = "SELECT * FROM employees WHERE employee_id = '$employee'";
    $query = $conn->query($sql);
    if ($query->num_rows < 1) {
        $_SESSION['error'] = 'Employee not found';
    } else {
        $row = $query->fetch_assoc();
        $employee_id = $row['id'];

        $sqlcode = "SELECT RIGHT(code_ca,5) as code FROM cashadvance ORDER BY id DESC LIMIT 1";
        $querycode = $conn->query($sqlcode);
        if ($querycode->num_rows <> 0) {
            $rowcode = $querycode->fetch_assoc();
            $code = $rowcode['code'] + 1;
        } else {
            $code = 1;
        }

        $tahun = date("y");
        $bulan = date("m");
        $get_id = str_pad($code, 5, "0", STR_PAD_LEFT);
        $code_ca = "CA$tahun$bulan$get_id";

        $sql = "INSERT INTO cashadvance (code_ca, employee_id, date_advance, amount, remaining_pay, status) VALUES ('$code_ca', '$employee_id', NOW(), '$amount', '$amount', 'pending')";
        if ($conn->query($sql)) {
            $_SESSION['success'] = 'Cash Advance added successfully';
        } else {
            $_SESSION['error'] = $conn->error;
        }
    }

    header('location: cashadvance.php');
} else if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $amount = $_POST['amount'];

    $sql = "UPDATE cashadvance SET amount = '$amount' WHERE id = '$id'";
    if ($conn->query($sql)) {
        $_SESSION['success'] = 'Cash advance updated successfully';
        header('location: cashadvance.php');
    } else {
        $_SESSION['error'] = $conn->error;
        header('location: cashadvance.php');
    }
} else if (isset($_POST['delete'])) {
    $id = $_POST['id'];
    $sqlcode = "SELECT code_ca FROM cashadvance WHERE id = '$id'";
    $querycode = $conn->query($sqlcode);
    $rowcode = $querycode->fetch_assoc();
    $code_ca = $rowcode['code_ca'];

    $sql = "DELETE FROM cashadvance WHERE id = '$id'";
    if ($conn->query($sql)) {
        $sqlpayment = "DELETE FROM cashadvance_payment WHERE code_ca = '$code_ca'";
        $conn->query($sqlpayment);

        $_SESSION['success'] = 'Cash advance deleted successfully';
    } else {
        $_SESSION['error'] = $conn->error;
    }

    header('location: cashadvance.php');
} else if (isset($_POST['id'])) {
    $id = $_POST['id'];
    $sql = "SELECT cashadvance.*, 
            cashadvance.id AS caid,
            employees.firstname,
            employees.lastname
        FROM cashadvance 
        LEFT JOIN employees on employees.id=cashadvance.employee_id 
        WHERE cashadvance.id='$id'";

    $query = $conn->query($sql);
    $row = $query->fetch_assoc();

    echo json_encode($row);
} else {
    $_SESSION['error'] = 'actions failed.';
    header('location: cashadvance.php');
}
