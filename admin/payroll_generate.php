<?php
include 'includes/session.php';
// Excel file name for download 
$filename = "Payroll - " . date('F') . " " . date('Y');

// Headers Excel
header("Content-Disposition: attachment; filename=$filename.xlsx");
header("Content-Type: application/vnd.ms-excel");

echo "<h3>$filename</h3>
    
<table border='1' cellpadding='5'>
    <tr>
        <th>Employee Name</th>
        <th>Position</th>
        <th>Total Attendance</th>
        <th>Total Overtime</th>
        <th>Percentage Bonus</th>
        <th>Salary Amount</th>
        <th>Gross Amount</th>
        <th>Overtime</th>
        <th>Bonus</th>
        <th>Deductions</th>
        <th>Cash Advance</th>
        <th>Net Pay</th>
    </tr>
    <tbody>";

    $sql = "SELECT SUM(amount) as total_amount FROM deductions";
    $query = $conn->query($sql);
    $drow = $query->fetch_assoc();
    $deduction = $drow['total_amount'];

    $sql = "SELECT  SUM(attendance.num_hr) AS total_hours, 
        (
            SELECT COUNT(*) as jml FROM attendance as a
            WHERE a.employee_id=attendance.employee_id AND DATE_FORMAT(a.date,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')
        ) AS total_attends,
        attendance.employee_id AS empid, employees.employee_id, employees.firstname, employees.lastname,  position.description AS position,position.rate, position.salary, position.bonus
        FROM attendance 
        LEFT JOIN employees ON employees.id=attendance.employee_id 
        LEFT JOIN position ON position.id=employees.position_id 
        WHERE DATE_FORMAT(attendance.date,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')
        GROUP BY attendance.employee_id 
        ORDER BY employees.lastname ASC, employees.firstname ASC";

    $query = $conn->query($sql);
    $total = 0;
    while ($row = $query->fetch_assoc()) {
        $employee_id = $row['empid'];

        $casql = "SELECT SUM(amount_payment) AS cashamount FROM cashadvance_payment WHERE employee_id='$employee_id' AND DATE_FORMAT(date_payment,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
        $caquery = $conn->query($casql);
        $carow = $caquery->fetch_assoc();
        $cashadvance = $carow['cashamount'];

        $otsql = "SELECT SUM(rate) AS cashovertime FROM overtime WHERE employee_id='$employee_id' AND DATE_FORMAT(date_overtime,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
        $otquery = $conn->query($otsql);
        $otrow = $otquery->fetch_assoc();
        $overtime = $otrow['cashovertime'];

        $otsql2 = "SELECT SUM(hours) AS hours_overtime FROM overtime WHERE employee_id='$employee_id' AND DATE_FORMAT(date_overtime,'%M')=DATE_FORMAT(CURRENT_DATE(),'%M')";
        $otquery2 = $conn->query($otsql2);
        $otrow2 = $otquery2->fetch_assoc();
        $total_overtime = $otrow2['hours_overtime'] == '' ? 0 : $otrow2['hours_overtime'];

        $salary = $row['salary'];
        $gross = $row['rate'] * $row['total_attends'];
        $total_deduction = $deduction + $cashadvance;
        $bonus = ($salary + $gross) * $row['bonus'] / 100;
        $net = ($salary + $gross + $overtime + $bonus) - $total_deduction;

        echo "
            <tr>
            <td>" . $row['firstname'] . " " . $row['lastname'] . "</td>
            <td>" . $row['position'] . "</td>
            <td>" . $row['total_attends'] . " days</td>
            <td>" . $total_overtime . " hours</td>
            <td>" . $row['bonus'] . " %</td>
            <td>Rp. " . number_format($salary, 2) . "</td>
            <td>Rp. " . number_format($gross, 2) . "</td>
            <td>Rp. " . number_format($overtime, 2) . "</td>
            <td>Rp. " . number_format($bonus, 2) . "</td>
            <td>Rp. " . number_format($deduction, 2) . "</td>
            <td>Rp. " . number_format($cashadvance, 2) . "</td>
            <td>Rp. " . number_format($net, 2) . "</td>
            </tr>
        ";
    }
echo "</tbody> 
</table>";

exit;
