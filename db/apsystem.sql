﻿# Host: localhost  (Version 5.7.33)
# Date: 2022-05-31 07:54:23
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "admin"
#

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "admin"
#

INSERT INTO `admin` VALUES (1,'admin','$2y$10$U4/qPW2j25anqXV55md94uA07ZZ/lECSQPvaDYalJIX9Oxj7H4INy','Admin','Payroll','admin.png','2018-04-30');

#
# Structure for table "attendance"
#

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `time_in` time DEFAULT NULL,
  `status` int(1) NOT NULL,
  `time_out` time DEFAULT NULL,
  `num_hr` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

#
# Data for table "attendance"
#

INSERT INTO `attendance` VALUES (88,1,1,'2022-03-19','21:43:00',0,'18:45:00',2.9666666666667),(89,1,1,'2022-03-01','06:15:00',1,'12:15:00',2.25),(90,1,1,'2022-04-04','18:30:00',0,'11:30:00',6),(91,1,NULL,'2022-04-05','18:45:00',0,'22:45:00',0.25),(94,4,NULL,'2022-04-03','20:15:00',0,'20:15:00',2.25),(96,1,NULL,'2022-04-03','20:15:00',0,'20:15:00',1.25),(97,3,NULL,'2022-04-04','22:15:00',0,'22:15:00',4.25),(98,4,NULL,'2022-04-04','22:58:33',0,NULL,NULL),(99,4,NULL,'2022-04-01','11:45:00',0,'11:45:00',0),(100,4,NULL,'2022-04-02','11:45:00',0,'11:45:00',0),(101,3,NULL,'2022-04-01','11:45:00',0,'11:45:00',0),(102,3,NULL,'2022-04-02','11:45:00',0,'11:45:00',0),(103,3,NULL,'2022-04-03','11:45:00',0,'11:45:00',0),(104,3,NULL,'2022-04-05','11:45:00',0,'11:45:00',0),(105,4,NULL,'2022-04-05','11:45:00',0,'11:45:00',0),(106,1,NULL,'2022-04-02','21:00:00',0,'21:00:00',2);

#
# Structure for table "attendance_type"
#

DROP TABLE IF EXISTS `attendance_type`;
CREATE TABLE `attendance_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "attendance_type"
#

INSERT INTO `attendance_type` VALUES (1,'Hadir'),(2,'Sakit/Izin'),(3,'Cuti');

#
# Structure for table "cashadvance"
#

DROP TABLE IF EXISTS `cashadvance`;
CREATE TABLE `cashadvance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_ca` varchar(20) NOT NULL,
  `employee_id` varchar(15) NOT NULL,
  `date_advance` date NOT NULL,
  `amount` double NOT NULL,
  `remaining_pay` double NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Data for table "cashadvance"
#

INSERT INTO `cashadvance` VALUES (4,'CA220400003','4','2022-04-08',50000,0,'paidoff'),(6,'CA220400004','3','2022-04-09',50000,47000,'pending');

#
# Structure for table "cashadvance_payment"
#

DROP TABLE IF EXISTS `cashadvance_payment`;
CREATE TABLE `cashadvance_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_pm` varchar(20) NOT NULL,
  `code_ca` varchar(20) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_payment` datetime NOT NULL,
  `amount` double NOT NULL,
  `amount_payment` double NOT NULL,
  `remaining_pay` double NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Data for table "cashadvance_payment"
#

INSERT INTO `cashadvance_payment` VALUES (4,'PM220400001','CA220400003',4,'2022-04-08 23:49:34',50000,50000,0,'paidoff'),(6,'PM220400002','CA220400004',3,'2022-04-09 00:27:08',50000,3000,47000,'pending');

#
# Structure for table "deductions"
#

DROP TABLE IF EXISTS `deductions`;
CREATE TABLE `deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "deductions"
#


#
# Structure for table "employees"
#

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(15) NOT NULL,
  `user_login` varchar(50) DEFAULT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` text,
  `birthdate` date DEFAULT NULL,
  `contact_info` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `position_id` int(11) NOT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `photo` varchar(200) NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "employees"
#

INSERT INTO `employees` VALUES (1,'ABC123456789','dd00','Lyndon','Bermoy','Surigao City','2018-04-02','09079373999','Male',3,4,'profile youtube1.jpg','2018-04-28'),(3,'DYE473869250','jonah','Jonah','Juarez','Surigao City','1992-05-02','09123456789','Female',2,2,'log.jpg','2018-04-30'),(4,'BTI458036279','malik00','Rizki','Malik','dws','1995-08-23','08823482234','Male',1,3,'','2022-03-20');

#
# Structure for table "overtime"
#

DROP TABLE IF EXISTS `overtime`;
CREATE TABLE `overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(15) NOT NULL,
  `hours` double NOT NULL,
  `rate` double NOT NULL,
  `date_overtime` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "overtime"
#

INSERT INTO `overtime` VALUES (1,'1',5,150,'2022-03-19'),(2,'4',5,50000,'2022-04-04'),(3,'4',3,30000,'2022-04-03'),(4,'4',3,45000,'2022-04-05');

#
# Structure for table "position"
#

DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(150) NOT NULL,
  `rate` double NOT NULL,
  `salary` double DEFAULT NULL,
  `bonus` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Data for table "position"
#

INSERT INTO `position` VALUES (1,'Programmer',200000,0,6),(2,'Writer',50000,0,5),(3,'Leader',0,5000000,7.5);

#
# Structure for table "salary"
#

DROP TABLE IF EXISTS `salary`;
CREATE TABLE `salary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "salary"
#


#
# Structure for table "schedules"
#

DROP TABLE IF EXISTS `schedules`;
CREATE TABLE `schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "schedules"
#

INSERT INTO `schedules` VALUES (1,'07:00:00','16:00:00'),(2,'08:00:00','17:00:00'),(3,'09:00:00','18:00:00'),(4,'10:00:00','19:00:00');
